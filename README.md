set speaking speed with SPEED environment variable.

This package requies espeak, opustools, and pdftotext

```
apt install opus-tools espeak poppler-utils
```

for example can add this to .bashrc
```
export SPEED="350"
```

if you use espeak-ng then can set that with
```
export ESPEAK="espeak-ng"
```

the default is 250, which is average reading speed. 


cliptosound plays from clipboard to xdg-open
  requires xclip, espeak and xdg-open

pdftoopus converts a file to 


Remember that you can configure how xdg-open opens a file using
```
mimeopen -d /tmp/psik
```

my recommendation is to use Other, and then type in mplayer


```
$ mimeopen -d /tmp/psik.wav 
Please choose a default application for files of type audio/x-wav

  1) mplayer  (mplayer-usercreated-1)
  2) Audacious  (audacious)
  3) Enqueue in SMPlayer  (smplayer_enqueue)
  4) SMPlayer  (smplayer)
  5) Spek  (spek)
  6) VLC media player  (vlc)
  7) Videos  (org.gnome.Totem)
  8) Other...

use application #8
use command: mplayer
```
